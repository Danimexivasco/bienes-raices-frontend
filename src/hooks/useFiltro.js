import React, { useState } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import styled from '@emotion/styled';
import {css} from '@emotion/core';

const Formulario = styled.form`
    width: 95%;
    display: flex;
    border: 1px solid #e1e1e1;
    max-width: 1200px;
    margin: 2rem auto 0 auto;

    @media (min-width:768px){
        justify-content: flex-start;
        max-width: 500px;
        margin-left: 1vw;
        
    }

`;

const Select = styled.select`
    flex: 1;
    padding: 1rem;
    appearance: none;
    border: none;
    font-family: 'Lato', sans-serif;
    width: 100%;
`;

const UseFiltro = () => {

    const [categoria, guardarCategoria] = useState("");

    const resultado = useStaticQuery(graphql`
        query {
            allStrapiCategorias {
                nodes{
                    id
                    nombre
                }
            }
        }
    `);



    const categorias = resultado.allStrapiCategorias.nodes;

    const FiltroUI = () => {

        return (
            <div
                css={css`
                    max-width: 1200px;
                    width: 100%;
                    margin: 2rem auto 0 auto;
                `}
            >

            <Formulario>
                <Select
                    onChange = {(e) => guardarCategoria(e.target.value)}
                    value = {categoria}
                >
                    <option value="" disabled>
                        --Selecciona una Categoría--
                    </option>
                    <option value="">Todas las propiedades</option>
                    {categorias.map(opcion => (
                        <option key={opcion.id} value={opcion.nombre}>{opcion.nombre}</option>
                        ))}
                </Select>
            </Formulario>
            <small
                css={css`
                    margin-left: 3vw;
                    @media (min-width:768px){    
                        margin-left: 1vw;
                    }
                `}
            ><i>Filtra por categoria</i></small>
            </div>
        )

    }

    return {
        categoria,
        FiltroUI
    }
}

export default UseFiltro;