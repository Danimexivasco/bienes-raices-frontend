import {graphql, useStaticQuery} from 'gatsby';

const usePropiedades = () => {
    const datos = useStaticQuery(graphql`
    query{
        allStrapiPropiedades{
          nodes{
            nombre
            id
            descripcion
            habitaciones
            wc
            precio
            estacionamiento
            categorias {
              nombre
            }
            agentes{
              nombre
              telefono
              email
            }
            imagen {
                sharp: childImageSharp {
                    fluid (maxWidth: 600, maxHeight: 400, quality: 90){
                        ...GatsbyImageSharpFluid_withWebp
                    }
                }
            }
          }
        }
      }
    `);
    return datos.allStrapiPropiedades.nodes.map(propiedad => ({
        nombre: propiedad.nombre,
        id: propiedad.id,
        descripcion: propiedad.descripcion,
        habitaciones: propiedad.habitaciones,
        wc: propiedad.wc,
        precio: propiedad.precio,
        estacionamiento: propiedad.estacionamiento,
        imagen: propiedad.imagen,
        categorias: propiedad.categorias,
        agentes: propiedad.agentes
    }))
}
 
export default usePropiedades;