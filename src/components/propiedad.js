import React from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/core';
import Image from 'gatsby-image';
import { graphql } from 'gatsby';
import Iconos from './iconos';
import Layout from './layout';


const Contenido = styled.div`
    max-width: 1200px;
    margin: 0 auto;
    width: 95%;
    p{
        padding-left: 1rem;
        padding-right: 1rem;
    }

    @media (min-width:768px){
        display: grid;
        grid-template-columns: 2fr 1fr;
        column-gap: 5rem;
        main p{
            padding: 0;
        }
    }
`;

const Sidebar = styled.aside`
    h2{
        text-align: left;
        margin-bottom: 1rem;
    }

    .precio{
        font-size: 2rem;
        color: #75AB00;
        margin-bottom: 2rem;
    }
    .agente{
        /* margin-top: 4rem; */
        margin-bottom: 2rem;
        border-radius: 2rem;
        background-color: #214768;
        padding: 3rem;
        color: #FFF;
        h2{
            margin-bottom: 1.5rem;
        }
    }
    p{
        margin: 0;
    }
`;

export const query = graphql`
    query ($id: String!) {
        allStrapiPropiedades (filter: {id: {eq: $id} }){
            nodes{
                nombre
                descripcion
                estacionamiento
                wc
                habitaciones
                precio
                agentes{
                    nombre
                    telefono
                    email
                }
                imagen {
                    sharp: childImageSharp {
                        fluid (maxWidth:1500, quality:90){
                            ...GatsbyImageSharpFluid_withWebp
                        }
                    }
                }
            }
        }
    }
`;

const Propiedad = ({ data: { allStrapiPropiedades: { nodes } } }) => {

    const { nombre, descripcion, wc, estacionamiento, habitaciones, imagen, precio, agentes } = nodes[0]

    return (
        <Layout>
            <h1
                css={css`
                    margin-top: 2rem;
                    margin-bottom: 2rem;
                `}
            >{nombre}</h1>
            <Contenido>
                <main>
                    <Image
                        fluid={imagen.sharp.fluid}
                    />
                    <p>{descripcion}</p>
                </main>
                <Sidebar>
                    <div className="agente">
                        <h2>Detalles Vendedor:</h2>
                        <p>{agentes.nombre}</p> 
                        <p>Tel: {agentes.telefono}</p> 
                        <p>Email: {agentes.email}</p> 
                    </div>
                    <h2>Precio:</h2>
                    <p className="precio">{precio} €</p>
                    <h2>Características de la propiedad:</h2>
                    <Iconos
                        wc={wc}
                        estacionamiento={estacionamiento}
                        habitaciones={habitaciones}
                    />
                </Sidebar>
            </Contenido>
        </Layout>
    );
}

export default Propiedad;