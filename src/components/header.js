import React, { useState } from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';
import { css } from '@emotion/core';
// import { CSSTransition, TransitionGroup } from 'react-transition-group';

import Navegacion from './navegacion';
import useWindowDimensions from '../hooks/useWindowDimensions';

const Header = () => {

    const [mostrando, guardarMostrando] = useState(false);
    const { width, height } = useWindowDimensions();

    const mostrarMenu = () => {
        guardarMostrando(!mostrando)
        // console.log('menu')
    }

    // Consultamos el logo
    const { logo } = useStaticQuery(graphql`
        query {
            logo: file(relativePath: {eq: "logo.svg"}){
            publicURL
            }
        }
      
    `);


    return (
        <header
            css={css`
                background-color: #222;
                padding: 1rem;
                color:  #FFF;
                
            `}
        >
            <div
                css={css`
                    max-width: 120rem;
                    margin: 0 auto;
                    text-align: center;
                    img{
                        margin-left: 5%;
                    }
                    svg{
                        fill: #FFF;
                        &:hover{
                            cursor: pointer;
                        }
                        @media (min-width: 768px){
                            display: none;
                        }
                    }

                    @media (min-width:768px){
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                        img{
                        margin-left: 0;
                        }
                    }
                `}
            >
                <div
                    css={css`
                        display: flex;
                        align-items: center;
                        svg{
                            margin-left: auto;
                            width: 3rem;
                            margin-right: 2rem;
                            /* margin-top: -10px; */
                        }
                    `}
                >
                    <Link to="/">
                        <img src={logo.publicURL} alt="Logo Bines Raices" />
                    </Link>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" onClick={() => mostrarMenu()}><path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z" /></svg>

                </div>

                {(width >= 768 || mostrando) && (
                    <>
                        {/* <CSSTransition in={mostrando} timeout={2000} classNames="nav"> */}
                        <Navegacion />
                            
                        {/* </CSSTransition> */}
                    </>
                )}
            </div>
        </header>

    );
}

export default Header;