import React from 'react';
import {Link} from 'gatsby';
import styled from '@emotion/styled';

const Nav = styled.nav`
    display: flex;
    flex-direction: column;
    padding-bottom: 2rem;
    align-items: center;
    /* padding-top: 2rem; */
    transition: opacity 0.6s ease-in-out;

    @media (min-width: 768px){
        padding: 0;
        display: flex;
        flex-direction: row;
    }

    
    /* &.nav-enter {
        opacity: 0;
    }
    &.nav-enter-active {
        opacity: 1;
    }
    &.nav-exit {
        opacity: 1;
    }
    &.nav-exit-active {
        opacity: 0;
    } */
    
`;

const NavLink = styled(Link)`
    color: #FFF;
    font-weight: 700;
    font-family: 'Lato', sans-serif;
    text-decoration: none;
    padding: .5rem;
    &.pagina-actual{
        /* color: #396077; */
        color: #295984;
    }

    @media (min-width: 768px){
        margin-right: 1rem;
        &:last-of-type{
            margin-right: 0;
        }
        &.pagina-actual{
        border-bottom: 2px solid #FFF;
        color: #FFF;
        }
    }
`;

const Navegacion = () => {


    return ( 
        <Nav>
            <NavLink to="/" activeClassName="pagina-actual">Inicio</NavLink>
            <NavLink to="/nosotros" activeClassName="pagina-actual">Nosotros</NavLink>
            <NavLink to="/propiedades" activeClassName="pagina-actual">Propiedades</NavLink>
        </Nav>
     );
}
 
export default Navegacion;