import React from 'react';
import {graphql, useStaticQuery} from 'gatsby';
import styled from '@emotion/styled';

const ListadoIconos = styled.ul`
    display: flex;
    justify-content: space-between;
    flex: 1;
    max-width: 500px;
    margin: 2rem auto 2rem auto;

    li{
        display: flex;
        /* padding-right: 1rem; */
        /* img{
             margin-right: 1rem; 
        } */
    }
    p{
        padding-left: 1.5rem;
    }
    
`;

const Iconos = ({wc, estacionamiento, habitaciones}) => {
    
    const iconos = useStaticQuery(graphql`
        query {
            allFile (filter: {relativeDirectory: {eq: "iconos"}}){
                edges{
                    node{
                        id
                        publicURL
                    }
                }
            }
        }
    `);
    const imagenesIconos = iconos.allFile.edges;
    
    return ( 
        
        <ListadoIconos>
            <li>
                <img src={imagenesIconos[3].node.publicURL} alt="imagen estacionamiento" />
                <p>{estacionamiento}</p>    
            </li>
            <li>
                <img src={imagenesIconos[1].node.publicURL} alt="imagen habitaciones" />
                <p>{habitaciones}</p>    
            </li>
            <li>
                <img src={imagenesIconos[2].node.publicURL} alt="imagen wc" />
                <p>{wc}</p>    
            </li>
        </ListadoIconos>
        
     );
}
 
export default Iconos;