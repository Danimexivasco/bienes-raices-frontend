import React, { useState, useEffect } from 'react';
import { css } from '@emotion/core';
import usePropiedades from '../hooks/usePropiedades';
import useFiltro from '../hooks/useFiltro';
import PropiedadPreview from './propiedadPreview';
import listadoPropiedadesCSS from '../css/ListadoPropiedades.module.css';
// import { useStaticQuery } from 'gatsby';

const ListadoPropiedades = () => {

    const resultado = usePropiedades();
    const [propiedades] = useState(resultado);
    const [filtradas, guardarFiltradas] = useState([]);

    // Filtrado de las propiedades
    const {categoria, FiltroUI} = useFiltro();
    // console.log(categoria)


    useEffect(() => {
        let montado = true;
        // eslint-disable-next-line
        if(categoria){
            const filtro = propiedades.filter(propiedad => propiedad.categorias.nombre === categoria);
            guardarFiltradas(filtro);
            // console.log(propiedades)
        }else{
            guardarFiltradas(propiedades)
        }
        return () => montado = false;
    }, [categoria])

    return (

        <>
            <h1
                css={css`
                margin-top: 4rem;
            `}
            >Nuestras Propiedades</h1>
            {/* <p>Buscas algo en especial?</p> */}
            {FiltroUI()}
            <ul className={listadoPropiedadesCSS.propiedades}>
                {filtradas.map(propiedad => (
                    <PropiedadPreview
                        key={propiedad.id}
                        propiedad={propiedad}
                    />
                ))}
            </ul>
        </>
    );
}

export default ListadoPropiedades;