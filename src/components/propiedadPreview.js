import React from 'react';
import styled from '@emotion/styled';
import Image from 'gatsby-image';
import { Link } from 'gatsby';
import urlSlug from 'url-slug';
import Iconos from './iconos';

const Card = styled.div`
    border: 1px solid #E1E1E1;
    margin-bottom: 1rem;
    img{
        max-width: 100%;
    }
`;

const Contenido = styled.div`
    padding: 2rem;
    h3{
        font-family: 'Lato', sans-serif;
        margin: 0 0 2rem 0;
        font-size: 1.4rem;
    }
    .precio{
        font-size: 2rem;
        color: #75AB00;
        padding-left: 0;
        margin-bottom: 1rem;
    }
    
`;

const Boton = styled(Link)`
    margin-top: 2rem;
    padding: 1rem;
    background-color: #75ab00;
    width: 100%;
    color: #FFF;
    display: block;
    text-align: center;
    text-decoration: none;
    font-weight: 700;
    text-transform: uppercase;
`;

const PropiedadPreview = ({ propiedad }) => {

    const { nombre, imagen, wc, estacionamiento, habitaciones, precio } = propiedad;
    return (

        <Card>
            <Image
                fluid={imagen.sharp.fluid}
            />
            <Contenido>
                <h3>{nombre}</h3>
                <p className="precio">{precio} €</p>
                <Iconos
                    wc={wc}
                    estacionamiento={estacionamiento}
                    habitaciones={habitaciones}
                />

                <Boton to={`../${urlSlug(nombre)}`}>
                    Visitar propiedad
                </Boton>
            </Contenido>
        </Card>
    );
}

export default PropiedadPreview;