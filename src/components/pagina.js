import React from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/core';
import Image from 'gatsby-image';
import { graphql } from 'gatsby';
import Layout from './layout';
import ListadoPropiedades from './listadoPropiedades';


const ContenidoPagina = styled.div`
    max-width: 1200px;
    margin: 0 auto 2rem auto;
    width: 95%;
    
    p{
        padding-left: 1rem;
        padding-right: 1rem;
    }

    @media (min-width:768px){
        display: grid;
        grid-template-columns: 2fr 1fr;
        column-gap: 5rem;
        main p{
            padding: 0;
        }
    }
`;


export const query = graphql`
    query($id: String!){
        allStrapiPaginas(filter: {id: { eq: $id}}){
            nodes{
                nombre
                contenido
                imagen{
                    sharp: childImageSharp {
                        fluid( maxWidth:1500, quality:90){
                            ...GatsbyImageSharpFluid_withWebp
                        }
                    }
                }
            }
        }
    }
`;

const Pagina = ({ data: { allStrapiPaginas: { nodes } } }) => {

    const {nombre, contenido, imagen} = nodes[0];

    return (
        <Layout>
            <main className="contenedor">
                <h1
                    css={css`
                        font-size: 5rem;
                        margin-top: 2rem;
                        margin-bottom: 2rem;
                    `}
                >{nombre}</h1>
                <ContenidoPagina>
                    <Image
                        fluid={imagen.sharp.fluid}
                    />
                    <p>{contenido}</p>
                </ContenidoPagina>
            </main>

            {nombre === "Propiedades" && (
                <ListadoPropiedades />
            )}

        </Layout>
    );
}

export default Pagina;