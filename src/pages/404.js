import React from "react";

import Layout from "../components/layout";
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {Link} from 'gatsby';

const LinkInicio = styled(Link)`
    text-decoration: none;
`;

const NotFoundPage = () => (
  <Layout>
    <h1>404 - NOT FOUND</h1>
    <div
        css={css`
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        `}
    >
        <p css={css`
            text-align: center;
        `}>Esta ruta no existe...</p>
        <LinkInicio to="/">
            Volver a inicio
        </LinkInicio>
    </div>

  </Layout>
)

export default NotFoundPage